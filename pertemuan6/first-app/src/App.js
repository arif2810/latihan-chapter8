import logo from './logo.svg';
import './App.css';
import Hello from './Hello';
// import ExpenseItem from './components/ExpenseItem';
import NewComponent from './components/NewComponent';

function App() {
  // const expenses = [
  //   {
  //     id: "e1",
  //     date: new Date(2022, 7, 20),
  //     title: "Pen",
  //     description: "Stabilo",
  //     amount: 1.23
  //   },
  //   {
  //     id: "e2",
  //     date: new Date(2022, 7, 20),
  //     title: "Pencil",
  //     description: "Stabilo",
  //     amount: 1.01
  //   },
  //   {
  //     id: "e3",
  //     date: new Date(2022, 7, 20),
  //     title: "Book",
  //     description: "Mindset",
  //     amount: 2.21
  //   },
  //   {
  //     id: "e4",
  //     date: new Date(2022, 7, 20),
  //     title: "Calendar",
  //     description: "Castel",
  //     amount: 4.41
  //   },
  // ]

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Hello/>
        <NewComponent/>
        
      </header>
    </div>
  );
}

export default App;
