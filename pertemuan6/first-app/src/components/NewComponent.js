import ExpenseItem from './ExpenseItem';
// import ExpenseDate from './ExpenseDate';
// import App from '../App';


function NewComponent(props){
  const expenses = [
    {
      id: "e1",
      date: new Date(2022, 7, 20),
      title: "Pen",
      description: "Stabilo",
      amount: 1.23
    },
    {
      id: "e2",
      date: new Date(2022, 7, 20),
      title: "Pencil",
      description: "Stabilo",
      amount: 1.01
    },
    {
      id: "e3",
      date: new Date(2022, 7, 20),
      title: "Book",
      description: "Mindset",
      amount: 2.21
    },
    {
      id: "e4",
      date: new Date(2022, 7, 20),
      title: "Calendar",
      description: "Castel",
      amount: 4.41
    },
  ]

  return(
    <header>
      <ExpenseItem
        date = {expenses[0].date}
        title = {expenses[0].title}
        description = {expenses[0].description}
        amount = {expenses[0].amount}
      />
      <ExpenseItem
        date = {expenses[1].date}
        title = {expenses[1].title}
        description = {expenses[1].description}
        amount = {expenses[1].amount}
      />
      <ExpenseItem
        date = {expenses[2].date}
        title = {expenses[2].title}
        description = {expenses[2].description}
        amount = {expenses[2].amount}
      />
      <ExpenseItem
        date = {expenses[3].date}
        title = {expenses[3].title}
        description = {expenses[3].description}
        amount = {expenses[3].amount}
      />
    </header>

  )
}

export default NewComponent;