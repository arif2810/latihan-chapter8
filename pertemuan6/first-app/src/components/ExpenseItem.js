import ExpenseDate from "./ExpenseDate";
import "./ExpenseItem.css";

function ExpenseItem(props){
  // const expenseDate = new Date(2022, 7, 20);
  // const expenseTitle = 'Pen';
  // const expenseDescription = 'Stabilo';
  // const expenseAmount = 1.23;

  return(
    <div className="expense-item">
      <ExpenseDate date = {props.date}/>
      {/* <div>{props.date.toISOString()}</div> */}
      <div className="expense-item__title">{props.title}</div>
      <div className="expense-item__description">
        <div>{props.description}</div>
        <div className="expense-item__price">${props.amount}</div>
      </div>
    </div>
  )
}

export default ExpenseItem;