const { Articles } = require("./models");

const query = {
  where: { id: 3 },
};

Articles.update(
  {
    title: "OOP",
    author: "Anna",
    body: "Mari belajar OOP!",
    approved: false,
  },
  query
)
  .then(() => {
    console.log("Artikel berhasil diupdate");
    process.exit();
  })
  .catch((err) => {
    console.log("Gagal mengupdate artikel!");
  });
