const express = require('express');
const router = express.Router();
const controller = require("../controllers");

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// Routes
router.get("/", controller.home);

const dashboard = require("./dashboard");
router.use("/dashboard", dashboard);

const auth = require("./auth");
router.use("/auth", auth);

router.use(controller.notFound);
router.use(controller.exception);

module.exports = router;
