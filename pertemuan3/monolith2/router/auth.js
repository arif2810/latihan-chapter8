const router = require("express").Router();
const { auth } = require("../controllers");

router.use((req, res, next) => {
  res.locals.layout = "layouts/authentication";
  next();
});
router.get("/login", auth.login);
router.post("/login", auth.api.login);

module.exports = router;
